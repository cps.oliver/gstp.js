# GSTP.js

A small library to make reading and interfacing with the Garmin Simple Text Output Protocol easier.

## Usage

```
const GSTPParser = require('gstp.js');

const gstpStr = '@040703112756S2933200E03017304g017+01149E0000N0000U0033';
const parsedData = new GSTPParser(gstpStr).parse();

console.log(parsedData);
```

Output:

```
{
  start: '@',
  year: 4,
  month: 7,
  day: 3,
  hour: 11,
  minute: 27,
  second: 56,
  latitudeHemisphere: 'S',
  latitudePosition: '2933200',
  longitudeHemisphere: 'E',
  longitudePosition: '03017304',
  positionStatus: 'g',
  horizontalPosnError: 17,
  altitudeSign: '+',
  altitude: 1149,
  ewVelocityDirection: 'E',
  ewVelocityMagnitude: 0,
  nsVelocityDirection: 'N',
  nsVelocityMagnitude: 0,
  verticalVelocityDirection: 'U',
  verticalVelocityMagnitude: 33
}
```

## Notes

- Basic verification is done to the passed GSTP string.
  - No attempt has been made to verify the `latitudePosition` and `longitudePosition` are
    correct. We are making the assumption that the GPS receiver is (or did) sending valid data.
- The GSTP only supports two digit years. Use common sense when operating on them.
  - GSTP.js does not pad years with zeros, thus `2004` is `4`.

## Future Features

- I need up update the library to accept invalid incoming data as underscores, as this is
  a defined behaviour of the protocol.

## Official Garmin Simple Text Protocol Documentation

```Simple Text Output Format:

The simple text (ASCII) output contains time, position, and velocity data in
the fixed width fields (not delimited) defined in the following table:

    FIELD DESCRIPTION:      WIDTH:  NOTES:
    ----------------------- ------- ------------------------
    Sentence start          1       Always '@'
    ----------------------- ------- ------------------------
   /Year                    2       Last two digits of UTC year
  | ----------------------- ------- ------------------------
  | Month                   2       UTC month, "01".."12"
T | ----------------------- ------- ------------------------
i | Day                     2       UTC day of month, "01".."31"
m | ----------------------- ------- ------------------------
e | Hour                    2       UTC hour, "00".."23"
  | ----------------------- ------- ------------------------
  | Minute                  2       UTC minute, "00".."59"
  | ----------------------- ------- ------------------------
   \Second                  2       UTC second, "00".."59"
    ----------------------- ------- ------------------------
   /Latitude hemisphere     1       'N' or 'S'
  | ----------------------- ------- ------------------------
  | Latitude position       7       WGS84 ddmmmmm, with an implied
  |                                 decimal after the 4th digit
  | ----------------------- ------- ------------------------
  | Longitude hemisphere    1       'E' or 'W'
  | ----------------------- ------- ------------------------
  | Longitude position      8       WGS84 dddmmmmm with an implied
P |                                 decimal after the 5th digit
o | ----------------------- ------- ------------------------
s | Position status         1       'd' if current 2D differential GPS position
i |                                 'D' if current 3D differential GPS position
t |                                 'g' if current 2D GPS position
i |                                 'G' if current 3D GPS position
o |                                 'S' if simulated position
n |                                 '_' if invalid position
  | ----------------------- ------- ------------------------
  | Horizontal posn error   3       EPH in meters
  | ----------------------- ------- ------------------------
  | Altitude sign           1       '+' or '-'
  | ----------------------- ------- ------------------------
  | Altitude                5       Height above or below mean
   \                                sea level in meters
    ----------------------- ------- ------------------------
   /East/West velocity      1       'E' or 'W'
  |     direction
  | ----------------------- ------- ------------------------
  | East/West velocity      4       Meters per second in tenths,
  |     magnitude                   ("1234" = 123.4 m/s)
V | ----------------------- ------- ------------------------
e | North/South velocity    1       'N' or 'S'
l |     direction
o | ----------------------- ------- ------------------------
c | North/South velocity    4       Meters per second in tenths,
i |     magnitude                   ("1234" = 123.4 m/s)
t | ----------------------- ------- ------------------------
y | Vertical velocity       1       'U' (up) or 'D' (down)
  |     direction
  | ----------------------- ------- ------------------------
  | Vertical velocity       4       Meters per second in hundredths,
   \    magnitude                   ("1234" = 12.34 m/s)
    ----------------------- ------- ------------------------
    Sentence end            2       Carriage return, '0x0D', and
                                    line feed, '0x0A'
    ----------------------- ------- ------------------------

If a numeric value does not fill its entire field width, the field is padded
with leading '0's (eg. an altitude of 50 meters above MSL will be output as
"+00050").

Any or all of the data in the text sentence (except for the sentence start
and sentence end fields) may be replaced with underscores to indicate
invalid data.
```
