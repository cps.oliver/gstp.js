/*
 * Licensed under the BSD-3 Clause license. Please see the LICENSE file.
 *
 * Written By: Connor Oliver
 * Date: February 21 2021
 */
const GSTPParser = require("../src/index");
const InvalidDataStringError = require("../src/errors/InvalidDataStringError");

describe("GSTPParser Tests", () => {
  describe("Invalid Data Tests", () => {
    test("Invalid Length - Short", () => {
      const gstpStr = "@040703112756S2933200E03017304g017+01149E0000N0000U00";

      expect(() => {
        new GSTPParser(gstpStr);
      }).toThrow(InvalidDataStringError);
    });

    test("Invalid Length - Long", () => {
      const gstpStr =
        "@040703112756S2933200E03017304g017+01149E0000N0000U00333";

      expect(() => {
        new GSTPParser(gstpStr);
      }).toThrow(InvalidDataStringError);
    });

    test("Invalid Start", () => {
      const gstpStr = "#040703112756S2933200E03017304g017+01149E0000N0000U0033";

      expect(() => {
        new GSTPParser(gstpStr).parse();
      }).toThrow(InvalidDataStringError);
    });

    test("Invalid Year - Not Number", () => {
      const gstpStr = "@AA0703112756S2933200E03017304g017+01149E0000N0000U0033";

      expect(() => {
        new GSTPParser(gstpStr).parse();
      }).toThrow(InvalidDataStringError);
    });

    test("Invalid Month - Out of Range High", () => {
      const gstpStr = "@041303112756S2933200E03017304g017+01149E0000N0000U0033";

      expect(() => {
        new GSTPParser(gstpStr).parse();
      }).toThrow(InvalidDataStringError);
    });

    test("Invalid Month - Out of Range Low", () => {
      const gstpStr = "@040003112756S2933200E03017304g017+01149E0000N0000U0033";

      expect(() => {
        new GSTPParser(gstpStr).parse();
      }).toThrow(InvalidDataStringError);
    });

    test("Invalid Day - Out of Range High", () => {
      const gstpStr = "@040700112756S2933200E03017304g017+01149E0000N0000U0033";

      expect(() => {
        new GSTPParser(gstpStr).parse();
      }).toThrow(InvalidDataStringError);
    });

    test("Invalid Day - Out of Range Low", () => {
      const gstpStr = "@040732112756S2933200E03017304g017+01149E0000N0000U0033";

      expect(() => {
        new GSTPParser(gstpStr).parse();
      }).toThrow(InvalidDataStringError);
    });

    test("Invalid Hour - Out of Range", () => {
      const gstpStr = "@040703242756S2933200E03017304g017+01149E0000N0000U0033";

      expect(() => {
        new GSTPParser(gstpStr).parse();
      }).toThrow(InvalidDataStringError);
    });

    test("Invalid Minute - Out of Range", () => {
      const gstpStr = "@040703116056S2933200E03017304g017+01149E0000N0000U0033";

      expect(() => {
        new GSTPParser(gstpStr).parse();
      }).toThrow(InvalidDataStringError);
    });

    test("Invalid Second - Out of Range", () => {
      const gstpStr = "@040703112760S2933200E03017304g017+01149E0000N0000U0033";

      expect(() => {
        new GSTPParser(gstpStr).parse();
      }).toThrow(InvalidDataStringError);
    });

    test("Invalid Latitude Hemisphere", () => {
      const gstpStr = "@040703112756Q2933200E03017304g017+01149E0000N0000U0033";

      expect(() => {
        new GSTPParser(gstpStr).parse();
      }).toThrow(InvalidDataStringError);
    });

    test("Invalid Latitude Magnitude", () => {
      const gstpStr = "@040703112756SA933200E03017304g017+01149E0000N0000U0033";

      expect(() => {
        new GSTPParser(gstpStr).parse();
      }).toThrow(InvalidDataStringError);
    });

    test("Invalid Longitude Hemisphere", () => {
      const gstpStr = "@040703112756S2933200Q03017304g017+01149E0000N0000U0033";

      expect(() => {
        new GSTPParser(gstpStr).parse();
      }).toThrow(InvalidDataStringError);
    });

    test("Invalid Longitude Magnitude", () => {
      const gstpStr = "@040703112756S2933200EZ3017304g017+01149E0000N0000U0033";

      expect(() => {
        new GSTPParser(gstpStr).parse();
      }).toThrow(InvalidDataStringError);
    });

    test("Invalid Position Status", () => {
      const gstpStr = "@040703112756S2933200E03017304z017+01149E0000N0000U0033";

      expect(() => {
        new GSTPParser(gstpStr).parse();
      }).toThrow(InvalidDataStringError);
    });

    test("Invalid Horizontal Position Error - String", () => {
      const gstpStr = "@040703112756S2933200E03017304gA17+01149E0000N0000U0033";

      expect(() => {
        new GSTPParser(gstpStr).parse();
      }).toThrow(InvalidDataStringError);
    });

    test("Invalid Horizontal Position Error - Float", () => {
      const gstpStr = "@040703112756S2933200E03017304g1.7+01149E0000N0000U0033";

      expect(() => {
        new GSTPParser(gstpStr).parse();
      }).toThrow(InvalidDataStringError);
    });

    test("Invalid Altitude Sign", () => {
      const gstpStr = "@040703112756S2933200E03017304g017*01149E0000N0000U0033";

      expect(() => {
        new GSTPParser(gstpStr).parse();
      }).toThrow(InvalidDataStringError);
    });

    test("Invalid Altitude - String", () => {
      const gstpStr = "@040703112756S2933200E03017304g017+A1149E0000N0000U0033";

      expect(() => {
        new GSTPParser(gstpStr).parse();
      }).toThrow(InvalidDataStringError);
    });

    test("Invalid Altitude - Float", () => {
      const gstpStr = "@040703112756S2933200E03017304g017+1.149E0000N0000U0033";

      expect(() => {
        new GSTPParser(gstpStr).parse();
      }).toThrow(InvalidDataStringError);
    });

    test("Invalid EW Direction Velocity", () => {
      const gstpStr = "@040703112756S2933200E03017304g017+01149Z0000N0000U0033";

      expect(() => {
        new GSTPParser(gstpStr).parse();
      }).toThrow(InvalidDataStringError);
    });

    test("Invalid EW Direction Magnitude - String", () => {
      const gstpStr = "@040703112756S2933200E03017304g017+01149EA000N0000U0033";

      expect(() => {
        new GSTPParser(gstpStr).parse();
      }).toThrow(InvalidDataStringError);
    });

    test("Invalid EW Direction Magnitude - Float", () => {
      const gstpStr = "@040703112756S2933200E03017304g017+01149E1.20N0000U0033";

      expect(() => {
        new GSTPParser(gstpStr).parse();
      }).toThrow(InvalidDataStringError);
    });

    test("Invalid NS Direction Velocity", () => {
      const gstpStr = "@040703112756S2933200E03017304g017+01149E0000Z0000U0033";

      expect(() => {
        new GSTPParser(gstpStr).parse();
      }).toThrow(InvalidDataStringError);
    });

    test("Invalid NS Direction Magnitude - String", () => {
      const gstpStr = "@040703112756S2933200E03017304g017+01149E0000NA000U0033";

      expect(() => {
        new GSTPParser(gstpStr).parse();
      }).toThrow(InvalidDataStringError);
    });

    test("Invalid NS Direction Magnitude - Float", () => {
      const gstpStr = "@040703112756S2933200E03017304g017+01149E0000N1.10U0033";

      expect(() => {
        new GSTPParser(gstpStr).parse();
      }).toThrow(InvalidDataStringError);
    });

    test("Invalid Vertical Direction Velocity", () => {
      const gstpStr = "@040703112756S2933200E03017304g017+01149E0000N0000Q0033";

      expect(() => {
        new GSTPParser(gstpStr).parse();
      }).toThrow(InvalidDataStringError);
    });

    test("Invalid Vertical Direction Magnitude - String", () => {
      const gstpStr = "@040703112756S2933200E03017304g017+01149E0000N0000UA033";

      expect(() => {
        new GSTPParser(gstpStr).parse();
      }).toThrow(InvalidDataStringError);
    });

    test("Invalid Vertical Direction Magnitude - Float", () => {
      const gstpStr = "@040703112756S2933200E03017304g017+01149E0000N0000U1.33";

      expect(() => {
        new GSTPParser(gstpStr).parse();
      }).toThrow(InvalidDataStringError);
    });
  });

  describe("Valid Data Tests", () => {
    test("Valid Start", () => {
      const gstpStr = "@040703112756S2933200E03017304g017+01149E0000N0000U0033";
      const parsedData = new GSTPParser(gstpStr).parse();

      expect(parsedData.start).toEqual("@");
    });

    test("Valid Year", () => {
      const gstpStr = "@040703112756S2933200E03017304g017+01149E0000N0000U0033";
      const parsedData = new GSTPParser(gstpStr).parse();

      expect(parsedData.year).toEqual(4);
    });

    test("Valid Month", () => {
      const gstpStr = "@040703112756S2933200E03017304g017+01149E0000N0000U0033";
      const parsedData = new GSTPParser(gstpStr).parse();

      expect(parsedData.month).toEqual(7);
    });

    test("Valid Day", () => {
      const gstpStr = "@040703112756S2933200E03017304g017+01149E0000N0000U0033";
      const parsedData = new GSTPParser(gstpStr).parse();

      expect(parsedData.day).toEqual(3);
    });

    test("Valid Hour", () => {
      const gstpStr = "@040703112756S2933200E03017304g017+01149E0000N0000U0033";
      const parsedData = new GSTPParser(gstpStr).parse();

      expect(parsedData.hour).toEqual(11);
    });

    test("Valid Minute", () => {
      const gstpStr = "@040703112756S2933200E03017304g017+01149E0000N0000U0033";
      const parsedData = new GSTPParser(gstpStr).parse();

      expect(parsedData.minute).toEqual(27);
    });

    test("Valid Second", () => {
      const gstpStr = "@040703112756S2933200E03017304g017+01149E0000N0000U0033";
      const parsedData = new GSTPParser(gstpStr).parse();

      expect(parsedData.second).toEqual(56);
    });

    test("Valid Latitude Hemisphere", () => {
      const gstpStr = "@040703112756S2933200E03017304g017+01149E0000N0000U0033";
      const parsedData = new GSTPParser(gstpStr).parse();

      expect(parsedData.latitudeHemisphere).toEqual("S");
    });

    test("Valid Latitude Position", () => {
      const gstpStr = "@040703112756S2933200E03017304g017+01149E0000N0000U0033";
      const parsedData = new GSTPParser(gstpStr).parse();

      expect(parsedData.latitudePosition).toEqual("2933200");
    });

    test("Valid Longitude Hemisphere", () => {
      const gstpStr = "@040703112756S2933200E03017304g017+01149E0000N0000U0033";
      const parsedData = new GSTPParser(gstpStr).parse();

      expect(parsedData.longitudeHemisphere).toEqual("E");
    });

    test("Valid Longitude Position", () => {
      const gstpStr = "@040703112756S2933200E03017304g017+01149E0000N0000U0033";
      const parsedData = new GSTPParser(gstpStr).parse();

      expect(parsedData.longitudePosition).toEqual("03017304");
    });

    test("Valid Position Status", () => {
      const gstpStr = "@040703112756S2933200E03017304g017+01149E0000N0000U0033";
      const parsedData = new GSTPParser(gstpStr).parse();

      expect(parsedData.positionStatus).toEqual("g");
    });

    test("Valid Horizontal Position Error", () => {
      const gstpStr = "@040703112756S2933200E03017304g017+01149E0000N0000U0033";
      const parsedData = new GSTPParser(gstpStr).parse();

      expect(parsedData.horizontalPosnError).toEqual(17);
    });

    test("Valid Altitude Sign", () => {
      const gstpStr = "@040703112756S2933200E03017304g017+01149E0000N0000U0033";
      const parsedData = new GSTPParser(gstpStr).parse();

      expect(parsedData.altitudeSign).toEqual("+");
    });

    test("Valid Altitude", () => {
      const gstpStr = "@040703112756S2933200E03017304g017+01149E0000N0000U0033";
      const parsedData = new GSTPParser(gstpStr).parse();

      expect(parsedData.altitude).toEqual(1149);
    });

    test("EW Velocity Direction", () => {
      const gstpStr = "@040703112756S2933200E03017304g017+01149E0000N0000U0033";
      const parsedData = new GSTPParser(gstpStr).parse();

      expect(parsedData.ewVelocityDirection).toEqual("E");
    });

    test("EW Velocity Magnitude", () => {
      const gstpStr = "@040703112756S2933200E03017304g017+01149E0000N0000U0033";
      const parsedData = new GSTPParser(gstpStr).parse();

      expect(parsedData.ewVelocityMagnitude).toEqual(0);
    });

    test("NS Velocity Direction", () => {
      const gstpStr = "@040703112756S2933200E03017304g017+01149E0000N0000U0033";
      const parsedData = new GSTPParser(gstpStr).parse();

      expect(parsedData.nsVelocityDirection).toEqual("N");
    });

    test("NS Velocity Magnitude", () => {
      const gstpStr = "@040703112756S2933200E03017304g017+01149E0000N0000U0033";
      const parsedData = new GSTPParser(gstpStr).parse();

      expect(parsedData.nsVelocityMagnitude).toEqual(0);
    });

    test("Vertical Velocity Direction", () => {
      const gstpStr = "@040703112756S2933200E03017304g017+01149E0000N0000U0033";
      const parsedData = new GSTPParser(gstpStr).parse();

      expect(parsedData.verticalVelocityDirection).toEqual("U");
    });

    test("Vertical Velocity Magnitude", () => {
      const gstpStr = "@040703112756S2933200E03017304g017+01149E0000N0000U0033";
      const parsedData = new GSTPParser(gstpStr).parse();

      expect(parsedData.verticalVelocityMagnitude).toEqual(33);
    });
  });
});
