/*
 * Licensed under the BSD-3 Clause license. Please see the LICENSE file.
 *
 * Written By: Connor Oliver
 * Date: February 21 2021
 */
const InvalidDataStringError = require("./errors/InvalidDataStringError");
const GSTPSchema = require("./schemas/GSTPSchema");

class GSTPParser {
  constructor(gstpString) {
    this.gstpString = gstpString.replace(/[\n\r]/g, "");

    this._initialBasicValidation();
  }

  _initialBasicValidation() {
    if (typeof this.gstpString !== "string") {
      throw new InvalidDataStringError(`Incorrect data type`);
    }

    if (this.gstpString.length !== 55) {
      throw new InvalidDataStringError(
        `Length of ${this.gstpString.length} is invalid`
      );
    }
  }

  parse() {
    const parsedData = {
      start: this.gstpString.substr(0, 1),
      year: Number(this.gstpString.substr(1, 2)),
      month: Number(this.gstpString.substr(3, 2)),
      day: Number(this.gstpString.substr(5, 2)),
      hour: Number(this.gstpString.substr(7, 2)),
      minute: Number(this.gstpString.substr(9, 2)),
      second: Number(this.gstpString.substr(11, 2)),
      latitudeHemisphere: this.gstpString.substr(13, 1),
      latitudePosition: this.gstpString.substr(14, 7),
      longitudeHemisphere: this.gstpString.substr(21, 1),
      longitudePosition: this.gstpString.substr(22, 8),
      positionStatus: this.gstpString.substr(30, 1),
      horizontalPosnError: Number(this.gstpString.substr(31, 3)),
      altitudeSign: this.gstpString.substr(34, 1),
      altitude: Number(this.gstpString.substr(35, 5)),
      ewVelocityDirection: this.gstpString.substr(40, 1),
      ewVelocityMagnitude: Number(this.gstpString.substr(41, 4)),
      nsVelocityDirection: this.gstpString.substr(45, 1),
      nsVelocityMagnitude: Number(this.gstpString.substr(46, 4)),
      verticalVelocityDirection: this.gstpString.substr(50, 1),
      verticalVelocityMagnitude: this.gstpString.substr(51, 4),
    };

    const validatedData = GSTPSchema.validate(parsedData);

    if (validatedData.error) {
      throw new InvalidDataStringError(validatedData.error);
    }

    return validatedData.value;
  }
}

module.exports = GSTPParser;
