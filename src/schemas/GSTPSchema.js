/*
 * Licensed under the BSD-3 Clause license. Please see the LICENSE file.
 *
 * Written By: Connor Oliver
 * Date: February 21 2021
 */
const Joi = require("@hapi/joi");

module.exports = Joi.object().keys({
  start: Joi.string()
    .valid(...["@"])
    .required(),
  year: Joi.number().integer().required(),
  month: Joi.number().integer().min(1).max(12).required(),
  day: Joi.number().integer().min(1).max(31).required(),
  hour: Joi.number().integer().min(0).max(23).required(),
  minute: Joi.number().integer().min(0).max(59).required(),
  second: Joi.number().integer().min(0).max(59).required(),
  latitudeHemisphere: Joi.string()
    .valid(...["N", "S"])
    .required(),
  latitudePosition: Joi.string().min(7).max(7).regex(/^\d+$/).required(),
  longitudeHemisphere: Joi.string()
    .valid(...["E", "W"])
    .required(),
  longitudePosition: Joi.string().min(8).max(8).regex(/^\d+$/).required(),
  positionStatus: Joi.string()
    .valid(...["d", "D", "g", "G", "_"])
    .required(),
  horizontalPosnError: Joi.number().integer().required(),
  altitudeSign: Joi.string()
    .valid(...["+", "-"])
    .required(),
  altitude: Joi.number().integer().required(),
  ewVelocityDirection: Joi.string()
    .valid(...["E", "W"])
    .required(),
  ewVelocityMagnitude: Joi.number().integer().required(),
  nsVelocityDirection: Joi.string()
    .valid(...["N", "S"])
    .required(),
  nsVelocityMagnitude: Joi.number().integer().required(),
  verticalVelocityDirection: Joi.string()
    .valid(...["U", "D"])
    .required(),
  verticalVelocityMagnitude: Joi.number().integer().required(),
});
