/*
 * Licensed under the BSD-3 Clause license. Please see the LICENSE file.
 *
 * Written By: Connor Oliver
 * Date: February 21 2021
 */

class InvalidDataStringError extends Error {
  constructor(message) {
    super(`Invalid data string was passed (${message})`);

    this.name = this.constructor.name;

    Error.captureStackTrace(this, this.constructor);
  }
}

module.exports = InvalidDataStringError;
